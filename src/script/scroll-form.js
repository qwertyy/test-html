function animateHTML () {
  var el, windowHeight;
  
  var init = function () {
    el = document.querySelector('.section__footer-form.need-anim');
    
    windowHeight = window.innerHeight;
    
    if (el) {
      _addEventHandlers();
      _checkPosition();
    }
  };
  
  function _addEventHandlers () {
    window.addEventListener('scroll', _checkPosition);
    window.addEventListener('resize', init);
  }
  
  function _removeEventHandlers () {
    window.removeEventListener('scroll', _checkPosition);
    window.removeEventListener('resize', init);
  }
  
  function _checkPosition () {
    var posFromTop = el.getBoundingClientRect().top;
    
    if (posFromTop - windowHeight <= 0) {
      _removeEventHandlers();
      
      el.classList.remove('need-anim');
      el.classList.add('anim-active');
    }
  }
  
  return {
    init: init
  };
}

animateHTML().init();